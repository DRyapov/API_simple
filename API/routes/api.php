<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//At this point we use no authentication
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Show all projects
Route::get('projects', 'APIController@projects');

//Show all developers
Route::get('developers', 'APIController@developers');

//Show all developerProjects objects
Route::get('developerProjects', 'APIController@developerProjects');

//Show all developerProjects objects for 1 project
Route::get('project_developers/{project_id}', 'APIController@projectDevelopers');

////Show all developerProjects objects for 1 developer
Route::get('developer_projects/{developer_id}', 'APIController@developerProjectsOneDev');

//Add project
Route::post('add_project', 'APIController@add_project');

//Add developer
Route::post('add_developer', 'APIController@add_developer');

//Add developerProject
Route::post('add_developerProject', 'APIController@add_developerProject');

