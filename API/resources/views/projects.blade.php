<table>
    <tr>
        <th>Name</th>
        <th>Status</th>
        <th>Deadline</th>
    </tr>
    @foreach ($projects as $project)
        <tr>
            <td>
                {{$project->name}}
            </td>
            <td>
                {{$project->status}}
            </td>
            <td>
                {{$project->deadline}}
            </td>
        </tr>
        {{--<p>Project name: {{$project->name}} with status {{$project->status}} with deadline {{$project->deadline}}</p>--}}
    @endforeach
</table>
