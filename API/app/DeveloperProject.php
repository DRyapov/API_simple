<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeveloperProject extends Model
{
    protected $fillable = ['project_id', 'developer_id', 'role'];
}
