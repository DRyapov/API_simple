<?php

namespace App\Http\Controllers;

use App\Developer;
use App\Project;
use App\DeveloperProject;
use Illuminate\Http\Request;

class APIController extends Controller
{
    //A function to return all the projects from the DB.
    public function projects(Request $request)
    {
        //Retrieve the projects from the DB
        $projects =  Project::all();

        //Check what the header expected content type is - if JSON, we return the response as JSON.
        if ($request->header('Content-Type') == 'application/json'){
            return $projects;
        }

        //We have a view to return if the expected content type is not JSON.
        return view('projects')->with('projects', $projects);
    }

    //A function to return all the developers from the DB
    public function developers(Request $request)
    {
        //Retrieve the developers from the DB
        $developers =  Developer::all();

        //Check what the header expected content type is - if JSON, we return the response as JSON.
        if ($request->header('Content-Type') == 'application/json'){
            return $developers;
        }

        //We have a view to return if the expected content type is not JSON.
        return view('developers')->with('developers', $developers);
    }

    //A function to return all the developerProject records from the DB
    public function developerProjects(Request $request)
    {
        //Retrieve the developerProjects objects from the DB
        $developerProjects =  DeveloperProject::all();

        //Check what the header expected content type is - if JSON, we return the response as JSON.
        if ($request->header('Content-Type') == 'application/json'){
            return $developerProjects;
        }

        //We have a view to return if the expected content type is not JSON.
        return view('developer_projects')->with('devProjects', $developerProjects);
    }

    //A function to return all the developerProject objects for 1 project from the DB
    public function projectDevelopers(Request $request, $project_id){

        //Retrieve the specific developerProjects from the DB
        $developerProjects =  DeveloperProject::where('project_id', $project_id)->get();

        //Check what the header expected content type is - if JSON, we return the response as JSON.
        if ($request->header('Content-Type') == 'application/json'){
            return $developerProjects;
        }

        //We have a view to return if the expected content type is not JSON.
        return view('developer_projects')->with('devProjects', $developerProjects);
    }

    //A function to return all the developerProject objects for 1 developer from the DB
    public function developerProjectsOneDev(Request $request, $developer_id){

        //Retrieve the specific developerProjects from the DB
        $developerProjects =  DeveloperProject::where('developer_id', $developer_id)->get();

        //Check what the header expected content type is - if JSON, we return the response as JSON.
        if ($request->header('Content-Type') == 'application/json'){
            return $developerProjects;
        }

        //We have a view to return if the expected content type is not JSON.
        return view('developer_projects')->with('devProjects', $developerProjects);
    }

    //A function to add a developer to the DB
    public function add_developer(Request $request){

        //Trying to find a record with the same e-mail -> if not found - the record is added to the DB
        $user = Developer::where('email', $request->email)->first();
        if ($user === null) {
            $developer = Developer::create($request->all());
            return response()->json($developer, 201);
        }

        //If found, return a meaningful error message with corresponding error code
        return response()->json('There is a record with the same e-mail', 409);
    }

    //A function to add a project to the DB
    public function add_project(Request $request){
        //Creating the project and saving it to the DB
        $developer = Project::create($request->all());
        return response()->json($developer, 201);

    }

    //A function to add a developer to the DB
    public function add_developerProject(Request $request){

        //Trying to find a record with the same developer_id and project_id -> if not found -
        //the record is added to the DB
        $developerProject = DeveloperProject::where('developer_id', $request->developer_id)
            ->where('project_id', $request->project_id)
            ->where(strtoupper('role'), strtoupper($request->role))->first();
        if ($developerProject === null) {
            $devProject = DeveloperProject::create($request->all());
            return response()->json($devProject, 201);
        }

        //If found, return a meaningful error message with corresponding error code
        return response()->json('There is a record with the same developer_id and project_id', 409);
    }
}
