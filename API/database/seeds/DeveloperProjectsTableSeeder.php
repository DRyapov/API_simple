<?php

use Illuminate\Database\Seeder;
use App\DeveloperProject;

class DeveloperProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DeveloperProject::create([
            'project_id' => 1,
            'developer_id' => 1,
            'role' => 'Back-End Developer'
        ]);
        DeveloperProject::create([
            'project_id' => 1,
            'developer_id' => 2,
            'role' => 'Full-stack developer'
        ]);

        for ($i = 0; $i < 10; $i++) {
            switch($i%3)
            {
                case '1' :
                    $role = 'Back-End developer';
                    break;
                case null :
                    $role = 'Front-End developer';
                    break;
                default :
                    $role = 'Full-stack developer';
            }

            $project_id = $i%5;
            DeveloperProject::create([
                'project_id' => $project_id,
                'developer_id' => $i,
                'role' => $role
            ]);
        }
    }
}
