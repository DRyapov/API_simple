<?php

use Illuminate\Database\Seeder;
use App\Project;
use Carbon\Carbon;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Project::create([
            'name' => 'Sofia Melius',
            'status' => 'live',
            'deadline' => Carbon::parse('2018-01-01')
        ]);
        Project::create([
            'name' => 'New Language',
            'status' => 'live',
            'deadline' => Carbon::parse('2018-02-01')
        ]);
        Project::create([
            'name' => 'Assemble Yourself',
            'status' => 'live',
            'deadline' => Carbon::parse('2017-09-01')
        ]);
        Project::create([
            'name' => 'Rocket Launch',
            'status' => 'in progress',
            'deadline' => Carbon::parse('2018-12-31')
        ]);
        Project::create([
            'name' => 'Graduation Project',
            'status' => 'planning',
            'deadline' => Carbon::parse('2019-01-31')
        ]);
    }
}
