<?php

use Illuminate\Database\Seeder;
use App\Developer;

class DevelopersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //A factory to help us with some name creation.
        $faker = \Faker\Factory::create();

        //Let's have some developers predefined.
        Developer::create([
            'name' => 'Dimitar Ryapov',
            'email' => 'dimitarryapov@gmail.com',
            'specialization' => 'Laravel',
        ]);
        Developer::create([
            'name' => 'Rubber Ducky',
            'email' => 'duck@pond.lake',
            'specialization' => 'Pair programmer',
        ]);

        //Here is an automated creation possibility, not used at the moment.
        for ($i = 0; $i < 20; $i++) {
            switch($i%3)
            {
                case '1' :
                    $specialization = 'C#';
                    break;
                case null :
                    $specialization = 'Java';
                    break;
                default :
                    $specialization = 'PHP';
            }
            Developer::create([
                'name' => $faker->name,
                'email' => $i.$faker->email,
                'specialization' => $specialization,
            ]);
        }
    }
}
