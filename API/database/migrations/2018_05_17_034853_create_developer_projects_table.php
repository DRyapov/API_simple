<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeveloperProjectsTable extends Migration
{
    /**
     * The migration that creates the table for connection between the developers and the projects they are working for.
     * Also, their role is defined in this table, as one developer can have different roles in different projects.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('developer_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('developer_id');
            $table->string('role');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('developer_projects');
    }
}
